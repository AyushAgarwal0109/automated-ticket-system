FROM ubuntu:latest
# Install wget and unzip
RUN apt-get update && \
    apt-get install -y wget unzip && \
    apt-get clean

# Install Python
RUN apt-get install -y python3 python3-pip && \
    pip3 install --upgrade pip && \
    python3 -V && \
    pip --version

# Install AWS CLI
RUN wget "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" && \
    unzip awscli-exe-linux-x86_64.zip && \
    ./aws/install && \
    rm -rf awscli-exe-linux-x86_64.zip ./aws && \
    aws --version

# Install Terraform
RUN wget "https://releases.hashicorp.com/terraform/1.5.0/terraform_1.5.0_linux_amd64.zip" && \
    ls && \
    unzip terraform_1.5.0_linux_amd64.zip && \
    mv terraform /usr/local/bin && \
    rm -rf terraform_1.5.0_linux_amd64.zip && \
    terraform --version

