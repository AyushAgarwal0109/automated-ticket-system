# ---------- Setting up Provider ------------
provider "aws" {
  region     = var.AWS_DEFAULT_REGION
  access_key = var.AWS_ACCESS_KEY_ID
  secret_key = var.AWS_SECRET_ACCESS_KEY
}

# ---------- Creating a VPC ------------
resource "aws_vpc" "terraformvpc" {
  cidr_block       = var.vpc_cidr
  instance_tenancy = "default"
  tags = {
    Name = "Terraform VPC"
  }
}

# ---------- Creating an Internet Gateway ------------
resource "aws_internet_gateway" "demogateway" {
  vpc_id = aws_vpc.terraformvpc.id
}

# ---------- Creating Subnets ------------
# Creating 1st subnet 
resource "aws_subnet" "subnet1" {
  vpc_id                  = aws_vpc.terraformvpc.id
  cidr_block              = var.subnet1_cidr
  map_public_ip_on_launch = true
  availability_zone       = "us-east-1a"
  tags = {
    Name = "Terraform subnet 1"
  }
}
# Creating 2nd subnet 
resource "aws_subnet" "subnet2" {
  vpc_id                  = aws_vpc.terraformvpc.id
  cidr_block              = var.subnet2_cidr
  map_public_ip_on_launch = true
  availability_zone       = "us-east-1b"
  tags = {
    Name = "Terraform subnet 2"
  }
}

# ---------- Creating Route Table ------------
#Creating Route Table
resource "aws_route_table" "route" {
  vpc_id = aws_vpc.terraformvpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.demogateway.id
  }
  tags = {
    Name = "Route to internet"
  }
}
#Creating Route Table Associations
resource "aws_route_table_association" "rt1" {
  subnet_id      = aws_subnet.subnet1.id
  route_table_id = aws_route_table.route.id
}
resource "aws_route_table_association" "rt2" {
  subnet_id      = aws_subnet.subnet2.id
  route_table_id = aws_route_table.route.id
}

# Creating Security Group for ELB
resource "aws_security_group" "sg1" {
  name        = "Security Group 1 for ELB"
  description = "Module"
  vpc_id      = aws_vpc.terraformvpc.id
  # Inbound Rules
  # HTTP access from anywhere
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  # HTTPS access from anywhere
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  # SSH access from anywhere
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  # Outbound Rules
  # Internet access to anywhere
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# ---------- Creating ELB ------------
resource "aws_elb" "web_elb" {
  name = "web-elb"
  security_groups = [
    "${aws_security_group.sg1.id}"
  ]
  subnets = [
    "${aws_subnet.subnet1.id}",
    "${aws_subnet.subnet2.id}"
  ]
  cross_zone_load_balancing = true
  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    interval            = 30
    target              = "HTTP:80/"
  }
  listener {
    lb_port           = 80
    lb_protocol       = "http"
    instance_port     = "80"
    instance_protocol = "http"
  }
}

# ---------- Creating Cloudwatch Alarm for ELB ------------
resource "aws_cloudwatch_metric_alarm" "elb_alarm" {
  alarm_name          = "elb-high-latency-alarm"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "2"
  metric_name         = "Latency"
  namespace           = "AWS/ELB"
  period              = "60"
  unit                = "Seconds"
  statistic           = "Average"
  threshold           = "5"
  dimensions = {
    LoadBalancerName = "${aws_elb.web_elb.name}"
  }
  alarm_description = "Alarm when ELB latency exceeds the threshold"
  alarm_actions     = []
}

# ---------- Setting up Launch Configuration ------------
resource "aws_launch_configuration" "web" {
  name_prefix                 = "web-"
  image_id                    = "ami-022e1a32d3f742bd8"
  instance_type               = "t2.micro"
  key_name                    = "tests"
  security_groups             = ["${aws_security_group.sg1.id}"]
  associate_public_ip_address = true
  lifecycle {
    create_before_destroy = true
  }
}

# Creating Security Group for EC2
resource "aws_security_group" "sg2" {
  name        = "Security Group for EC2"
  description = "Module"
  vpc_id      = aws_vpc.terraformvpc.id
  # Inbound Rules
  # HTTP access from anywhere
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  # HTTPS access from anywhere
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  # SSH access from anywhere
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  # Outbound Rules
  # Internet access to anywhere
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# ---------- Creating AutoScaling Group ------------
resource "aws_autoscaling_group" "web" {
  name             = "${aws_launch_configuration.web.name}-asg"
  min_size         = 1
  desired_capacity = 1
  max_size         = 2

  health_check_type = "ELB"
  load_balancers = [
    "${aws_elb.web_elb.id}"
  ]
  launch_configuration = aws_launch_configuration.web.name
  enabled_metrics = [
    "GroupMinSize",
    "GroupMaxSize",
    "GroupDesiredCapacity",
    "GroupInServiceInstances",
    "GroupTotalInstances"
  ]
  metrics_granularity = "1Minute"
  vpc_zone_identifier = [
    "${aws_subnet.subnet1.id}",
    "${aws_subnet.subnet2.id}"
  ]
  # Required to redeploy without an outage.
  lifecycle {
    create_before_destroy = true
  }
  tag {
    key                 = "Name"
    value               = "web"
    propagate_at_launch = true
  }
}

# ---------- Creating AutoScaling Policies and CloudWatch Alarms ------------
resource "aws_autoscaling_policy" "web_policy_up" {
  name                   = "web_policy_up"
  scaling_adjustment     = 1
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 300
  autoscaling_group_name = aws_autoscaling_group.web.name
}

resource "aws_cloudwatch_metric_alarm" "web_cpu_alarm_up" {
  alarm_name          = "web_cpu_alarm_up"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "2"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "120"
  statistic           = "Average"
  threshold           = "70"
  dimensions = {
    AutoScalingGroupName = "${aws_autoscaling_group.web.name}"
  }
  alarm_description = "This metric monitor EC2 instance CPU utilization"
  alarm_actions     = ["${aws_autoscaling_policy.web_policy_up.arn}"]
}

resource "aws_autoscaling_policy" "web_policy_down" {
  name                   = "web_policy_down"
  scaling_adjustment     = -1
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 300
  autoscaling_group_name = aws_autoscaling_group.web.name
}

resource "aws_cloudwatch_metric_alarm" "web_cpu_alarm_down" {
  alarm_name          = "web_cpu_alarm_down"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = "2"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "120"
  statistic           = "Average"
  threshold           = "30"
  dimensions = {
    AutoScalingGroupName = "${aws_autoscaling_group.web.name}"
  }
  alarm_description = "This metric monitor EC2 instance CPU utilization"
  alarm_actions     = ["${aws_autoscaling_policy.web_policy_down.arn}"]
}

# ---------- Creating Custom CloudWatch Alarms ------------
# NetworkIn Alarm
resource "aws_cloudwatch_metric_alarm" "network_in_alarm" {
  alarm_name          = "network-in-threshold"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "2"
  metric_name         = "NetworkIn"
  namespace           = "AWS/EC2"
  period              = "120"
  unit                = "Bytes"
  statistic           = "Sum"
  threshold           = "100"
  dimensions = {
    AutoScalingGroupName = "${aws_autoscaling_group.web.name}"
    LoadBalancerName     = "${aws_elb.web_elb.name}"
  }
  alarm_description = "Alarm when NetworkIn exceeds the threshold"
  alarm_actions     = []
}

# Disk Usage Alarm
resource "aws_cloudwatch_metric_alarm" "disk_usage_alarm" {
  alarm_name          = "disk-usage-threshold"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "2"
  metric_name         = "DiskSpaceUtilization"
  namespace           = "AWS/EC2"
  period              = "120"
  unit                = "Percent"
  statistic           = "Average"
  threshold           = "70"
  dimensions = {
    AutoScalingGroupName = "${aws_autoscaling_group.web.name}"
    LoadBalancerName     = "${aws_elb.web_elb.name}"
  }
  alarm_description = "Alarm when disk usage exceeds the threshold"
  alarm_actions     = [] # Add any specific actions to trigger when the threshold is exceeded
}

