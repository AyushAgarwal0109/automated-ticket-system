#!/bin/bash

spinUpEnv() {
    #Spinning Up AWS EC2 Instance using Terraform with AWS ELB and AWS AutoScaling
    terraform init
    wait
    terraform plan
    wait
    terraform apply -auto-approve
    wait
}

autoScale() {
    # Set the alarm name and state
    alarm_name="web_cpu_alarm_up"
    alarm_state="ALARM" # Replace with "OK" or "ALARM" to trigger or resolve the alarm

    # Trigger the alarm state
    aws cloudwatch set-alarm-state --alarm-name "$alarm_name" --state-value "$alarm_state" --state-reason "Manually triggering the alarm"
    echo "Auto Scaling..."
    wait
}

changeELBthreshold() {
    # Set the alarm name and state
    alarm_name="elb-high-latency-alarm"
    alarm_state="ALARM" # Replace with "OK" or "ALARM" to trigger or resolve the alarm

    # Trigger the alarm state
    aws cloudwatch set-alarm-state --alarm-name "$alarm_name" --state-value "$alarm_state" --state-reason "Manually triggering the alarm"
    echo "Balancing load..."
    wait

    # Detect the trigger and increase the threshold
    source scripts/checkup-alarms.sh
    wait
}

changeInstanceType() {
    # Fetch the instance ID
    local instance_id=$(aws ec2 describe-instances \
        --query 'Reservations[*].Instances[*].[InstanceId]' \
        --filters Name=instance-state-name,Values=running \
        --output text)

    local new_instance_type=""

    # Get the current instance type
    local current_instance_type=$(aws ec2 describe-instances \
        --instance-ids "$instance_id" \
        --query "Reservations[0].Instances[0].InstanceType" \
        --output text)

    # Determine the new instance type
    if [ "$current_instance_type" == "t2.micro" ]; then
        new_instance_type="t3.small"
    elif [ "$current_instance_type" == "t3.small" ]; then
        new_instance_type="t3.medium"
    fi

    # Stop the instance
    aws ec2 stop-instances --instance-ids "$instance_id"
    # Wait for the instance to stop
    aws ec2 wait instance-stopped --instance-ids "$instance_id"
    # Modify the instance type
    aws ec2 modify-instance-attribute --instance-id "$instance_id" --instance-type "{\"Value\": \"$new_instance_type\"}"
    # Start the instance
    aws ec2 start-instances --instance-ids "$instance_id"
    # Wait for the instance to start
    aws ec2 wait instance-running --instance-ids "$instance_id"

    echo "Instance type changed to $new_instance_type"
}

# Read the Excel file
dataFile="./Sample.csv"

# Iterate over each row in the Excel file
while IFS=',' read -r column1 column2; do
    # Access the data from each column and perform operations as needed
    if [[ "${column2,,}" =~ "environment" ]] && [[ "${column2,,}" =~ "not available" ]]; then
        echo "Column 1: $column1"
        echo "Column 2: $column2"
        spinUpEnv
    else
        if [[ "${column2,,}" =~ "memory issues" ]]; then
            echo "Column 1: $column1"
            echo "Column 2: $column2"
            changeInstanceType
        else
            if [[ "${column2,,}" =~ "running very slowly" ]] || [[ "${column2,,}" =~ "performance issues" ]]; then
                echo "Column 1: $column1"
                echo "Column 2: $column2"
                autoScale
            fi
        fi
    fi
done <"$dataFile"

# Wait for all background processes to complete
wait
