import json
import os

# Function to parse JSON into HTML using json and join functions and write it to a file
# Parameters:
#   json_file: Path to the JSON file
#   html_file: Path to the HTML file to be created
def parse_json_to_html(json_file):
    with open(json_file) as file:
        json_data = json.load(file)

    def process_object(data):
        if isinstance(data, dict):
            return "<ul>\n" + "\n".join(f"<li><b>{key}:</b> {process_value(value)}</li>" for key, value in data.items()) + "\n</ul>"
        return ""

    def process_array(data):
        if isinstance(data, list):
            return "<ul>\n" + "\n".join(f"<li>{process_value(value)}</li>" for value in data) + "\n</ul>"
        return ""

    def process_value(data):
        if isinstance(data, dict):
            return process_object(data)
        elif isinstance(data, list):
            return process_array(data)
        return str(data)

    html_content = process_value(json_data)
    return html_content

# Path to the directory containing the JSON files
json_dir = "json_docs"

# Path to the directory where the HTML files will be created
html_dir = "html_docs"

# Create the HTML directory if it doesn't exist
if not os.path.exists(html_dir):
    os.makedirs(html_dir)

# Loop over the JSON files in the directory
html_content = ""
for filename in os.listdir(json_dir):
    if filename.endswith(".json"):
        json_file = os.path.join(json_dir, filename)
        html_content += "<div class=\"row card\">" + "<h1>"+ filename + "</h1>" + "<div class=\"card-body\">" + parse_json_to_html(json_file) + "</div></div>"

html_file = os.path.join(html_dir, "document.html")
html_content = "<html><body>\n<link rel=\"stylesheet\" href=\"https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css\" integrity=\"sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65\" crossorigin=\"anonymous\">\n<div class=\"container\">" + html_content + "</div></body></html>"
with open(html_file, "w") as output_file:
    output_file.write(html_content)
print("Parsing completed. You can now see the updated document in html_docs dir and the old document in old_doc dir.")
