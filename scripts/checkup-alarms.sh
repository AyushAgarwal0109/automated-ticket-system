#!/bin/bash

# Function to alter the Cloudwatch metrics on alarm trigger
function handleTrigger() {
    # Set the alarm name and state
    local alarm_name="$1"
    local alarm_namespace="$2"
    local metric_name="$3"
    local alarm_reason="$4"
    local alarm_evaluation_periods="$5"
    local alarm_period="$6"

    echo "CloudWatch alarm '$alarm_name' is triggered!"
    echo "Trigger message: $alarm_reason"

    # Setting up threshold
    local threshold=0
    if [[ "$alarm_name" == "elb-high-latency-alarm" ]]; then
        threshold=4
    elif [[ "$alarm_name" == "network-in-threshold" ]]; then
        threshold=120
    else
        threshold=80
    fi

    echo "$alarm_namespace $alarm_name $metric_name $threshold $alarm_evaluation_periods $alarm_period"
    # Alter CloudWatch metrics using put-metric-data
    aws cloudwatch put-metric-alarm \
        --alarm-name $alarm_name \
        --namespace $alarm_namespace \
        --metric-name $metric_name \
        --threshold $threshold \
        --evaluation-periods $alarm_evaluation_periods \
        --comparison-operator "GreaterThanOrEqualToThreshold" \
        --period $alarm_period \
        --statistic "Average"
}
# Set the names of the CloudWatch alarms to monitor (separated by space)
alarms=("elb-high-latency-alarm" "network-in-threshold" "disk-usage-threshold")

for curr_alarm in "${alarms[@]}"; do
    # Retrieve the state and description of the specified alarms
    alarm_info=$(aws cloudwatch describe-alarms --alarm-names $curr_alarm --query 'MetricAlarms[0].[AlarmName, Namespace, MetricName, StateValue, StateReason, EvaluationPeriods, Period]' --output text)

    # Convert the alarm info to an array
    IFS=$'\n' read -ra alarm_array <<<"$alarm_info"

    # Iterate over each alarm
    for alarm_line in "${alarm_array[@]}"; do
        IFS=$'\t' read -ra alarm <<<"$alarm_line"
        alarm_name="${alarm[0]}"
        alarm_namespace="${alarm[1]}"
        metric_name="${alarm[2]}"
        alarm_state="${alarm[3]}"
        alarm_reason="${alarm[4]}"
        alarm_evaluation_periods="${alarm[5]}"
        alarm_period="${alarm[6]}"
    done

    # Haddle the triggered alarms
    if [[ "$alarm_state" == "ALARM" ]]; then
        handleTrigger "$alarm_name" "$alarm_namespace" "$metric_name" "$alarm_reason" "$alarm_evaluation_periods" "$alarm_period"
    fi
done
