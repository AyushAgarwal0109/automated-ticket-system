#!/bin/bash

# Function to fetch EC2 instance configuration
function output_config() {
    local instance_id="$1"
    local ec2_output_file="$2"
    local asg_output_file="$3"
    local elb_output_file="$4"

    # Retrieve EC2 configurations
    local ec2Description=$(aws ec2 describe-instances \
        --instance-ids "$instance_id" \
        --region us-east-1 \
        --output json)

    # Retrieve Auto Scaling Group Name from EC2 instance
    local asgName=$(aws ec2 describe-instances \
        --instance-ids "$instance_id" \
        --query 'Reservations[0].Instances[0].Tags[?Key==`aws:autoscaling:groupName`].Value' \
        --output text)

    # Retrieve Auto Scaling Group configurations
    local asgDescription=$(aws autoscaling describe-auto-scaling-groups \
        --auto-scaling-group-names $asgName \
        --query 'AutoScalingGroups[0]' \
        --output json)

    # Retrieve Load Balancer Names associated with Auto Scaling Group
    local lbNames=$(aws autoscaling describe-auto-scaling-groups \
        --auto-scaling-group-names $asgName \
        --query 'AutoScalingGroups[0].LoadBalancerNames' \
        --output text)

    # Retrieve Load Balancer configurations
    local elbDescription=""
    for lbName in $lbNames; do
        lbDescription=$(aws elb describe-load-balancers \
            --load-balancer-name $lbName \
            --output json)
        elbDescription+="$lbDescription"
    done

    echo $ec2Description >>$ec2_output_file
    echo $asgDescription >>$asg_output_file
    echo $elbDescription >>$elb_output_file
    echo "Configurations for instance $instance_id has been documented"
}

# Fetch the instance ID
instance_id=$(aws ec2 describe-instances \
    --query 'Reservations[*].Instances[*].[InstanceId]' \
    --filters Name=instance-state-name,Values=running \
    --output text)

# Set the output file
ec2_output_file="ec2_configuration.json"
asg_output_file="asg_configuration.json"
elb_output_file="elb_configuration.json"

# Fetch configurations and write to file
output_config "$instance_id" "$ec2_output_file" "$asg_output_file" "$elb_output_file"
mv *.json json_docs
